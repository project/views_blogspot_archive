<?php

namespace Drupal\Tests\views_blogspot_archive\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests for the views_blogspot_archive module.
 *
 * @group views_blogspot_archive
 */
class ViewsBlogspotArchiveOliveroTest extends WebDriverTestBase {

  use NodeCreationTrait;
  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'views_blogspot_archive',
    'views',
    'node',
    'block',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user1;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();
    $this->user1 = $this->DrupalCreateUser();
    $this->createContentType([
      'type' => 'page',
    ]);
    // Create 12 nodes in 2023.
    for ($i = 1; $i <= 12; $i++) {
      $this->createNode([
        'status' => TRUE,
        'type' => 'page',
        'created' => random_int(1672557954, 1704008865),
      ]);
    }
    // Create 8 nodes in 2024.
    for ($i = 1; $i <= 8; $i++) {
      $this->createNode([
        'status' => TRUE,
        'type' => 'page',
        'created' => random_int(1704095265, 1709279265),
      ]);
    }
    $this->drupalPlaceBlock('views_block:viewsblogspot_archive-block_1');
  }

  /**
   * Tests main functionality in Olivero theme.
   */
  public function testMainFunctionalityOlivero() {
    $this->drupalLogin($this->user1);
    $link = $this->getSession()->getPage()->find('xpath', '//span/a[text()="2023"]')->getParent();
    $this->assertNotNull($link);
    $link->click();
    $this->assertTrue($link->hasClass('caret-down'));
    $li = $link->getParent();
    $this->assertNotNull($li);
    $ul_active = $li->find('css', 'ul.active');
    $this->assertNotNull($ul_active);
    $month_li = $ul_active->find('css', 'li:first-child');
    $this->assertNotNull($month_li);
    $month_span = $month_li->find('css', 'span.caret');
    $this->assertNotNull($month_span);
    $month_span->click();
    $month_ul_active = $month_li->find('css', 'ul.active');
    $this->assertNotNull($month_ul_active);
    $node_li = $month_ul_active->find('css', 'li:first-child');
    $this->assertNotNull($node_li);
    $node_a = $node_li->find('css', 'a');
    $this->assertNotNull($node_a);
    $node_href = $node_a->getAttribute('href');
    $this->assertStringContainsString('/node/', $node_href);
  }

}
