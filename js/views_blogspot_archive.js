((Drupal) => {
  Drupal.behaviors.views_blogspot_archive = {
    attach(context) {
      once('views_blogspot_archive', '.caret', context).forEach(
        // eslint-disable-next-line func-names
        function (toggle) {
          // eslint-disable-next-line func-names
          toggle.addEventListener('click', function () {
            this.parentElement.querySelector('ul').classList.toggle('active');
            this.classList.toggle('caret-down');
          });
        },
      );
    },
  };
})(Drupal);
