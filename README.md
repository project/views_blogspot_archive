# CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

## INTRODUCTION

This module provides a blogger(blogspot) like archive style option for Views.
You can set the field based on which archive need to be created in views style
settings.

* For a full description of the module, visit the project page:
  <https://drupal.org/project/views_blogspot_archive>

* To submit bug reports and feature suggestions, or to track changes:
  <https://drupal.org/project/issues/views_blogspot_archive>


## REQUIREMENTS

Drupal core views.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

Follow example view attached for setting references(Block display for archive
and Page display for views archive result).


## MAINTAINERS

* Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
